@extends('layouts.userapp')
@section('content')
<section id="main-section">

    <section class="module-top">
        <div class="container">
            <div class="row no-gutter">
                <!--========== BEGIN .COL-MD-8 ==========-->
                <div class="col-md-8">
                    <!--========== BEGIN .FLEXSLIDER ==========-->
                    <div class="flexslider">
                        <ul class="slides">
                            @foreach ($posts as $item)
                            <li> <img src="/uploads/{{ $item->image }}" alt="">
                                <div class="meta">
                                    <h3>{{ $item->title }}</h3>
                                    <h4> {{ $item->created_at->format('d-M-Y') }}</h4>
                                </div>
                            </li>
                            @endforeach
                        </ul>
                    </div>
                    <!--========== END .FLEXSLIDER ==========-->
                </div>
                <!--========== END .COL-MD-8 ==========-->
                <!--========== BEGIN .COL-MD-4 ==========-->
                <div class="col-md-4">
                    <!-- Begin .top-sidebar-add-place -->
                    <div class="top-sidebar-add-place"> <a
                            href=""
                            ><img src="/uploads/{{ $banner->image }}" alt=""></a> </div>
                    <!-- End .top-sidebar-add-place -->
                    <!--========== BEGIN #WEATHER ==========-->

                    <!--========== END  #WEATHER ==========-->
                </div>
                <!--========== END .COL-MD-4 ==========-->
            </div>
        </div>
    </section>
    <section class="module highlight" wp-site-content wp-body-class wp-site-name wp-title wp-site-desc wp-header-image>
        <div class="container">
            <div class="module-title">
                <h3 class="title"><span class="bg-1">World News</span></h3>
                <h3 class="subtitle">Watch the latest news</h3>
            </div>
            <!--========== BEGIN .ROW ==========-->
            <div class="row no-gutter">
                <!--========== BEGIN .COL-MD-6 ==========-->
                <div class="col-sm-6 col-md-6">
                    <!--========== BEGIN .NEWS ==========-->
                    <div class="news">
                        <!-- Begin .item -->
                        @foreach ($posts as $item)

                        <div class="item">
                            <div class="item-image-1">
                                <a class="img-link" href="#">
                                    <img class="img-responsive img-full" src="/uploads/{{ $item->image }}" alt="">
                                </a>
                                <span><a class="label-3" href="politics.html">{{ $item->category->name }}</a></span>
                            </div>
                            <div class="item-content">
                                <div class="title-left title-style04 underline04">
                                    <h3><a href="/postdetail/{{ $item->id }}">{{ $item->title }}</h3>
                                </div>
                                <p><a href="/postdetail/{{ $item->id }}" target="_blank" class="external-link">{!!
                                        substr($item->description, 0,150) !!}</a></p>
                                <div>
                                    <a href="/postdetail/{{ $item->id }}" target="_blank"><span
                                            class="read-more">Politics</span></a>
                                </div>
                            </div>
                        </div>
                        @endforeach
                        <!-- End .item -->
                    </div>
                    <!--========== END .NEWS ==========-->
                </div>

                <!-- Begin .item -->

                <!-- End .item -->
            </div>
            <!--========== END .NEWS ==========-->
        </div>
        <!--========== END .COL-MD-6 ==========-->
        </div>
        </div>
    </section>
    <!--========== BEGIN .MODULE ==========-->
    <section class="module">
        <div class="container">
            <div class="row no-gutter">
                <!--========== BEGIN .COL-MD-8 ==========-->
                <div class="col-md-8">
                    <!--========== BEGIN .NEWS ==========-->
                    <div class="news">
                        <div class="module-title">
                            <h3 class="title"><span class="bg-11">{{ $category->name}}</span></h3>
                            <h3 class="subtitle">Latest News in details</h3>
                        </div>
                        @foreach ($secondposts as $item)
                        <!-- Begin .item-->
                        <div class="item">
                            <div class="item-image-2">
                                <a class="img-link" href="/postdetail/{{ $item->id }}">
                                    <img class="img-responsive img-full" src="/uploads/{{ $item->image }}" alt="">
                                </a>
                                <span><a class="label-2" href="politics.html">{{ $item->category->name }}</a></span>
                            </div>
                            <div class="item-content">
                                <div class="title-left title-style04 underline04">
                                    <h3><a href="/postdetail/{{ $item->id }}"><strong>Woman</strong> in Mission
                                            Hills</a>
                                    </h3>
                                </div>
                                <p><a href="/postdetail/{{ $item->id }}">{!! substr($item->description, 0,150) !!} </a>
                                </p>
                                <div>
                                    <a href="/postdetail/{{ $item->id }}" target="_blank"><span
                                            class="read-more">{{ $item->category->name }}</span></a>
                                </div>
                            </div>
                        </div>

                        @endforeach

                        <!-- End .item-->

                        <!-- End .item-->
                    </div>
                    <!--========== End .NEWS ==========-->
                </div>
                <!--========== End .COL-MD-8 ==========-->
                <!--========== BEGIN .COL-MD-4 ==========-->
                <div class="col-md-4">
                    <!-- Begin .sidebar-add-place -->
                    <div class="sidebar-add-place">
                        <a href="#"
                            target="_blank">
                            <img src="/uploads/{{ $banner->image }}" alt="">
                        </a>
                    </div>
                    <!-- End .sidebar-add-place -->
                    <!-- Begin .block-title-1 -->
                    <div class="block-title-1">
                        <h3><a href="#"><strong>24h News</strong> Feed</a></h3>
                    </div>
                    <!-- End .block-title-1 -->
                    <!--========== BEGIN .SIDEBAR-NEWSFEED ==========-->
                    <div class="sidebar-newsfeed">
                        <!-- Begin .newsfeed -->
                        <div class="newsfeed-3">
                            <ul>
                                @foreach ($breakings as $item)
                                <li>
                                    <div class="item">
                                        <div class="item-image">
                                            <a class="img-link" href="/postdetail/{{ $item->id }}">
                                                <img class="img-responsive img-full" src="/uploads/{{ $item->image }}"
                                                    alt="">
                                            </a>
                                        </div>
                                        <div class="item-content">
                                            <h4 class="ellipsis"><a
                                                    href="/postdetail/{{ $item->id }}">{{ $item->title }}</a></h4>
                                            <p class="ellipsis"><a href="/postdetail/{{ $item->id }}">{!!
                                                    substr($item->description,0,60) !!}</a></p>
                                        </div>
                                    </div>
                                </li>
                                @endforeach
                            </ul>
                        </div>
                        <!-- End .newsfeed -->
                    </div>
                    <!--========== END .SIDEBAR-NEWSFEED ==========-->
                </div>
                <!--========== END .COL-MD-4 ==========-->
            </div>
        </div>
    </section>
    <!--========== END .MODULE ==========-->
    @endsection
