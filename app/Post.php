<?php

namespace App;

use App\Tag;
use App\Category;
use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    protected $fillable = ['title', 'description', 'category_id', 'image'];


    public function tags()
    {
        return $this->belongsToMany(Tag::class)->withPivot('tag_id');
    }

    public function category()
    {
        return $this->belongsTo(Category::class);
    }
}
