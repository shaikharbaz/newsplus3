<?php

namespace App\Http\Controllers;

use App\Banner;
use App\Category;
use App\Post;
use Illuminate\Http\Request;

class PageController extends Controller
{
    public function home()
    {
        $category = Category::first();
        $secondposts = Post::where('category_id', 1)->get();
        $posts = Post::latest()->take(4)->get();
        $banner = Banner::first();
        $breakings = Post::latest()->take(6)->get();
        return view('user.home', compact('posts', 'category', 'secondposts', 'breakings', 'banner'));
    }

    public function postdetail(Post $post)
    {
        $banner = Banner::first();
        $categories = Category::with('posts')->get();
        return view('user.postdetail', compact('post', 'categories', 'banner'));
    }


    public function category(Category $category)
    {
        $banner = Banner::first();
        $headlines = Post::where('category_id', $category->id)->latest()->take(7)->get();
        return view('user.category', compact('category', 'headlines', 'banner'));
    }
}
