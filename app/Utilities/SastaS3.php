<?php

namespace App\Utilities;

use RuntimeException;
use GuzzleHttp\Client;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class SastaS3
{
    protected $tmpDir;

    public function __construct($tmpDir)
    {
        if (!is_dir(public_path($tmpDir))) {
            throw new RuntimeException("$tmpDir does not exist");
        }

        $this->tmpDir = trim($tmpDir, '/') . '/';
    }

    /**
     * multiple file upload to the server
     *
     * @param array $files
     * @param string $directory
     * @return array
     */
    public function uploadMultiple(array $files, string $directory): array
    {
        $names = [];
        foreach ($files as $file) {
            $names[] = $this->upload($file, $directory);
        }

        return $names;
    }

    /**
     * upload the file to the server
     * returns uploaded file name
     * 
     * @param UploadedFile $file
     * @param string $directory
     * 
     * @return string
     */
    public function upload(UploadedFile $file, string $directory)
    {
        // check if the uploaded file exists
        if (!file_exists($file->getRealPath())) {
            throw new RuntimeException("File $file not found");
        }

        // store the file locally temporarily
        $name = $this->storeLocally($file);

        $client = new Client([
            'base_uri' => config('s3.server')
        ]);

        // send data to the server (file name, temporary directory of your application, directory to which upload the file)
        $response = $client->post("/uploads", [
            'form_params' => [
                'name' => $name,
                'tmpDir' => $this->tmpDir,
                'directory' => $directory
            ]
        ]);

        $decoded = json_decode(
            $response->getBody()->getContents(),
            true
        );

        return $decoded['path'];
    }

    /**
     * Move files to store them locally for further processing
     * returns locally stored file name
     *
     * @param UploadedFile $file
     * @return void
     */
    protected function storeLocally(UploadedFile $file)
    {
        $name = uniqid() . '.' . $file->getClientOriginalExtension();
        $path = $this->tmpDir . $name;

        $from = fopen($file->getRealPath(), 'rb');
        $to = fopen(public_path($path), 'wb');

        stream_copy_to_stream($from, $to);

        fclose($to);
        fclose($from);

        return $name;
    }
}
