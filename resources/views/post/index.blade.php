@extends('layouts.adminapp')
@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12 col-sm-12" style="margin-top:30px">
            <div class="box">
                <div class="box-header">
                    <h4 class="box-title">News</h4>

                    <div class="box-tools">
                        <div class="input-group input-group-sm hidden-xs" style="width: 150px;margin-left:1000px">
                            <a class="btn btn-sm btn-primary" href="{{ route('posts.create') }}">
                                News
                                <i class="fa fa-plus"></i>
                            </a>
                        </div>
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body table-responsive no-padding">
                    <table class="table table-hover">
                        <tr>
                            <th>ID</th>
                            <th>Name</th>
                            <th>Category</th>
                            <th>Tags</th>
                            <th>Date</th>
                            <th colspan="2" class="text-center">Action</th>
                        </tr>
                        @foreach ($posts as $post)
                        <tr>
                            <td>{{ $post->id }}</td>
                            <td>{{ $post->title }}</td>
                            <td>{{ $post->category->name }}</td>
                            <td>@foreach ($post->tags as $item)
                                    {{ $item->name }},
                            @endforeach</td>
                            <td>{{ $post->created_at->format('d-M-Y') }}</td>
                            <td>
                            <a  class="btn btn-primary btn-sm"  href="{{ route('posts.edit',$post->id) }}">
                                    Edit
                            </a>
                                <!-- Modal -->

                            </td>
                            <td>
                                <button type="button" class="btn btn-danger btn-sm" data-toggle="modal"
                                    data-target="#remove{{ $post->id }}">
                                    Remove
                                </button>

                                <form action="{{ route('categories.destroy',$post->id) }}" method="POST">
                                    @csrf
                                    @method('delete')
                                    <div class="modal fade" id="remove{{ $post->id }}" tabindex="-1" role="dialog"
                                        aria-labelledby="remove{{ $post->id }}Label" aria-hidden="true">
                                        <div class="modal-dialog" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title" id="remove{{ $post->id }}Label">
                                                        {{ $post->name }}</h5>
                                                    <button type="button" class="close" data-dismiss="modal"
                                                        aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                                <div class="modal-body">
                                                    Are you sure you want to remove this iteam ?
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-secondary btn-sm"
                                                        data-dismiss="modal">Close</button>
                                                    <button type="submit" class="btn btn-danger btn-sm">Remove</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </td>
                        </tr>
                        @endforeach
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
    </div>
</div>
@endsection
