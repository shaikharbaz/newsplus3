@extends('layouts.adminapp')
@section('content')
<div class="container">
        <div class="row">
            <div class="col-md-8 col-sm-12" style="margin-top:20px">
                <div class="box">
                    <div class="box-header">
                        <h5 class="box-title"> Tags</h5>

                        <div class="box-tools">
                            <div class="input-group input-group-sm hidden-xs" style="width: 150px;">
                            </div>
                        </div>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body table-responsive no-padding">
                        <table class="table table-hover">
                            <tr>
                                <th>ID</th>
                                <th>Name</th>
                                <th>Date</th>
                                <th colspan="2" class="text-center">Action</th>
                            </tr>
                            @foreach ($tags as $tag)
                            <tr>
                                <td>{{ $tag->id }}</td>
                                <td>{{ $tag->name }}</td>
                                <td>{{ $tag->created_at->format('d-M-Y') }}</td>
                                <td>
                                    <button type="button" class="btn btn-primary btn-sm" data-toggle="modal"
                                        data-target="#edit{{ $tag->id }}">
                                        Edit
                                    </button>
                                    <!-- Modal -->
                                    <form action="{{ route('tags.update', $tag->id) }}" method="post">
                                        @csrf
                                        @method('put')
                                        <div class="modal fade" id="edit{{ $tag->id }}" tabindex="-1" role="dialog"
                                            aria-labelledby="edit{{ $tag->id }}Label" aria-hidden="true">
                                            <div class="modal-dialog" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h5 class="modal-title" id="edit{{ $tag->id }}Label">Edit
                                                            ward {{ $tag->name }}</h5>
                                                        <button type="button" class="close" data-dismiss="modal"
                                                            aria-label="Close">
                                                            <span aria-hidden="true">&times;</span>
                                                        </button>
                                                    </div>
                                                    <div class="modal-body">

                                                        <div class="form-group">
                                                            <label for="name">Name</label>
                                                            <input type="text" class="form-control" placeholder="Enter Name"
                                                                value="{{ $tag->name }}" name="name">
                                                            @if ($errors->has('name'))
                                                            <strong
                                                                class="text-danger">{{ $errors->first('name') }}</strong>
                                                            @endif
                                                        </div>
                                                        <input type="hidden" name="type" value="{{ $tag->type }}">
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-secondary btn-sm"
                                                            data-dismiss="modal">Close</button>
                                                        <button type="submit" class="btn btn-info btn-sm">update</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </td>
                                <td>
                                    <button type="button" class="btn btn-danger btn-sm" data-toggle="modal"
                                        data-target="#remove{{ $tag->id }}">
                                        Remove
                                    </button>

                                    <form action="{{ route('tags.destroy',$tag->id) }}" method="POST">
                                        @csrf
                                        @method('delete')
                                        <div class="modal fade" id="remove{{ $tag->id }}" tabindex="-1" role="dialog"
                                            aria-labelledby="remove{{ $tag->id }}Label" aria-hidden="true">
                                            <div class="modal-dialog" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h5 class="modal-title" id="remove{{ $tag->id }}Label">
                                                            {{ $tag->name }}</h5>
                                                        <button type="button" class="close" data-dismiss="modal"
                                                            aria-label="Close">
                                                            <span aria-hidden="true">&times;</span>
                                                        </button>
                                                    </div>
                                                    <div class="modal-body">
                                                        Are you sure you want to remove this iteam ?
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-secondary btn-sm"
                                                            data-dismiss="modal">Close</button>
                                                        <button type="submit" class="btn btn-danger btn-sm">Remove</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </td>
                            </tr>
                            @endforeach
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
            <div class="col-md-4 col-md-4" style="margin-top:20px">
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <h5 class="box-title">Create New  Tag</h5>
                        </div>
                        <form role="form" action="{{ route('tags.store') }}" method="post">
                            @csrf
                            <div class="box-body">
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Name</label>
                                    <input type="text" class="form-control" id="exampleInputEmail1" placeholder="Enter name"
                                        name="name">
                                    @if($errors->has('name'))
                                    <strong class="text-danger">{{ $errors->first('name') }}</strong>
                                    @endif
                             </div>
                            </div>
                            <!-- /.box-body -->

                            <div class="box-footer">
                                <button type="submit" class="btn btn-primary btn-block">Submit</button>
                            </div>
                        </form>
                    </div>
                 </div>
              </div>
         </div>
        @endsection
