<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'PageController@home');
Route::get('/postdetail/{post}', 'PageController@postdetail');
Route::get('/category/{category}', 'PageController@category');



Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::middleware('auth')->group(function () {
    //tags route
    Route::resource('tags', 'TagController');
    //category route
    Route::resource('categories', 'CategoryController');
    //Posts route
    Route::resource('posts', 'PostController');

    //banner routes
    Route::resource('banners', 'BannerController');
});
