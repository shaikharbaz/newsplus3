@extends('layouts.adminapp')
@section('css')
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.10/css/select2.min.css" rel="stylesheet" />
@endsection
@section('content')
<div class="row">
    <!-- left column -->
    <div class="col-md-2">
    </div>
    <div class="col-md-6">
        <!-- general form elements -->
        <div class="box box-primary">
            <div class="box-header with-border" style="margin-top:20px">
                <h5 class="box-title">Edit Post</h5>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form role="form" method="post" action="{{ route('posts.update',$post->id) }}" enctype="multipart/form-data">
                @csrf
                @method('put')
                <div class="box-body">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Title</label>
                                <input type="text" class="form-control" id="" placeholder="Enter Title"
                                    name="title" value="{{ $post->title }}">
                                @if($errors->has('title'))
                                <strong class="text-danger">{{ $errors->first('title') }}</strong>
                                @endif
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Image</label>
                                <input type="file" class="form-control" id="" placeholder="Enter Title"
                                    name="image" value="{{ $post->image }}">
                                @if($errors->has('image'))
                                <strong class="text-danger">{{ $errors->first('image') }}</strong>
                                @endif
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Category</label>
                                <select class="form-control" name="category_id">
                                    <option class="form-control" value="{{ $post->category->id }}">{{ $post->Category->name }}</option>
                                    @foreach ($categories as $item)
                                    <option class="form-control" value="{{ $item->id }}">{{ $item->name }}</option>
                                    @endforeach
                                </select>
                                @if($errors->has('category_id'))
                                <strong class="text-danger">{{ $errors->first('category_id') }}</strong>
                                @endif
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="form-group">
                                <select class="form-control" name="tag_id[]" id="mySelect2" multiple="multiple">
                                    @foreach ($post->tags as $item)
                                    <option value="{{ $item->id }}">{{ $item->name }}</option>
                                    @endforeach
                                </select>
                                @if($errors->has('tag_id'))
                                <strong class="text-danger">{{ $errors->first('tag_id') }}</strong>
                                @endif
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Post</label>
                                <textarea class="form-control" id="my-editor" name="description">{{ $post->description }}</textarea>
                                @if($errors->has('description'))
                                <strong class="text-danger">{{ $errors->first('description') }}</strong>
                                @endif
                            </div>
                        </div>
                    </div>
                    <!-- /.box-body -->

                    <div class="box-footer">
                        <button type="submit" class="btn btn-primary btn-block">Submit</button>
                    </div>
            </form>
        </div>
        <!-- /.box -->

    </div>
    <!-- /.box-body -->
</div>
<!-- /.box -->
</div>
<!--/.col (right) -->
</div>
<!-- /.row -->
</section>
<!-- /.content -->
</div>
<!-- /.content-wrapper -->
@endsection

@section('js')
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.10/js/select2.min.js"></script>
<script src="//cdn.ckeditor.com/4.6.2/standard/ckeditor.js"></script>
<script>
    var _token = $('input[name="_token"]').val();
    $("#mySelect2").select2({
        placeholder: 'Select Tags',
        ajax: {
            url: "{{ route('tags.index') }}",
            method: "GET",
            dataType: 'json',
            delay: 250,
            data: function (params) {
                return {
                    search: params.term,
                }
            },
            processResults: function (data) {
                return {
                    results: data.map((item) => ({
                        id: item.id,
                        text: item.name
                    }))
                };
            },

            cache: true
        },
        escapeMarkup: function (markup) {
            return markup;
        }, // let our custom formatter work
        minimumInputLength: 1,
    });
</script>
<script>
    var options = {
        filebrowserImageBrowseUrl: '/laravel-filemanager?type=Images',
        filebrowserImageUploadUrl: '/laravel-filemanager/upload?type=Images&_token=',
        filebrowserBrowseUrl: '/laravel-filemanager?type=Files',
        filebrowserUploadUrl: '/laravel-filemanager/upload?type=Files&_token='
    };

</script>
<script>
    CKEDITOR.replace('my-editor', options);

</script>
@endsection
