<?php

namespace App\Providers;

use App\Tag;
use App\Category;
use App\Post;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        View()->composer('layouts.userapp', function ($view) {

            $view->with(['categories' => Category::all()]);
        });

        View()->composer('layouts.userapp', function ($view) {

            $view->with(['tags' => Tag::take(20)->get()]);
        });
        View()->composer('layouts.userapp', function ($view) {

            $view->with(['posts' => Post::take(4)->get()]);
        });
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
