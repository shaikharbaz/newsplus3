@extends('layouts.userapp')
@section('content')
<!--========== BEGIN #MAIN-SECTION ==========-->
<section id="main-section">
    <!--========== BEGIN .CONTAINER ==========-->
    <div class="container">
        <!-- Begin .breadcrumb-line -->
        <div class="breadcrumb-line">
            <ul class="breadcrumb">
                <li>
                    <h5><a href="/">Home</a></h5>
                </li>
            <li class="active">{{ $category->name}}</li>
            </ul>
        </div>
        <!-- End .breadcrumb-line -->

        <!--========== BEGIN .MODULE ==========-->
        <section class="module-top">
            <!--========== BEGIN .CONTAINER ==========-->
            <div class="container">
                <!--========== BEGIN .BREAKING-NEWS ==========-->
                <!-- Begin .outer -->

                <!-- End #outer -->
                <!--========== END .BREAKING-NEWS ==========-->
                <div class="row no-gutter">
                    <!--========== BEGIN .COL-MD-8 ==========-->
                    <div class="col-md-8">
                        <!--========== BEGIN #SCHEDULE-SLIDER ==========-->

                        <!--========== END #SCHEDULE-SLIDER ==========-->
                        <!--========== BEGIN .TABS ==========-->
                        <!-- Begin .panel panel-flat -->
                        <div class="panel panel-flat">
                            <!-- Begin .panel-body -->
                            <div class="panel-body">
                                <!-- Begin .tabbable -->
                                <div class="tabbable">
                                    <!-- Begin .nav nav-tabs nav-tabs-solid nav-justified -->

                                    <!-- End .nav nav-tabs nav-tabs-solid nav-justified -->
                                    <!--========== BEGIN .TABS 1 ==========-->
                                    <!-- Begin .tab-content -->
                                    <div class="tab-content">
                                        <!-- Begin .tab-pane active" id="solid-justified-tab1 -->
                                        <div class="tab-pane active" id="solid-justified-tab1">
                                            <!--========== BEGIN .ROW ==========-->
                                            <div class="row no-gutter">
                                                <!-- Begin .title-style01 -->
                                                <div class="title-style01">

                                                </div>
                                                <!-- End .title-style01 -->
                                                @foreach ($category->posts as $item)
                                                <div class="news">
                                                    <!-- Begin .col-md-1 -->
                                                    <div class="col-xs-12 col-sm-1 col-md-1">
                                                        <h3 class="schedule-hour">06:00</h3>
                                                    </div>
                                                    <!-- Begin .col-md-11 -->
                                                    <div class="col-xs-12 col-sm-11 col-md-11">
                                                        <div class="item">
                                                            <div class="item-image-2"><a class="img-link"
                                                                    href="/postdetail/{{ $item->id }}"><img
                                                                        class="img-responsive img-full"
                                                                        src="/uploads/{{ $item->image }}" alt=""></a>
                                                            </div>
                                                            <div class="item-content">
                                                                <div class="title-left title-style04 underline04">
                                                                    <h3><a
                                                                            href="/postdetail/{{ $item->id }}"><strong>{{$item->title}}</strong></a>
                                                                    </h3>
                                                                </div>
                                                                <p class="subtitle">
                                                                    {{ $item->created_at->format('d-M-Y') }}</p>
                                                                <p>{!! substr($item->description,0,20) !!}</p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                @endforeach

                                                <!-- End .row -->
                                            </div>
                                        </div>

                                    </div>
                                    <!-- End .tab-content -->
                                </div>
                                <!-- End .tabbable -->
                            </div>
                            <!-- End .panel-body -->
                        </div>
                        <!-- End .panel panel-flat -->
                    </div>
                    <!--========== END .COL-MD-8 ==========-->
                    <!--========== BEGIN .COL-MD-4 ==========-->
                    <div class="col-md-4">
                        <!-- Begin .block-title-1 -->
                        <div class="block-title-1">
                            <h3><strong>Headlines</strong></h3>
                        </div>
                        <!-- End  .block-title-1 -->
                        <!--========== BEGIN .SIDEBAR-POST ==========-->
                        <div class="sidebar-post">
                            <ul>
                                @foreach ($headlines as $item)
                                <li>
                                    <div class="item">
                                        <div class="item-image"><a class="img-link"
                                                href="/postdetail/{{ $item->id }}"><img class="img-responsive img-full"
                                                    src="/uploads/{{ $item->image }}" alt=""></a></div>
                                        <div class="item-content">
                                            <h3>{{ $item->title }}</h3>
                                            <p class="ellipsis"><a href="/postdetail/{{ $item->id }}">{!!
                                                    substr($item->description,0,20) !!}</a></p>
                                        </div>
                                    </div>
                                </li>
                                @endforeach

                            </ul>
                        </div>

        </section>
        <!--========== END .MODULE ==========-->
    </div>
    <!--========== END .CONTAINER ==========-->
</section>
<!--========== END #MAIN-SECTION ==========-->
@endsection
