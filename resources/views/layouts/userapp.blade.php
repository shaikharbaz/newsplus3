<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="description" content="News Plus - Only Truth">
    <meta name="author" content="News Plus - Only Truth">
    <!-- Mobile Metas -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Site Title  -->
    <title>News Plus - Only Truth</title>
    <!-- Favicon -->
    <link rel="shortcut icon" href="img/favicon.png" type="image/x-icon" />
    <!-- Web Fonts  -->
    <link
        href="https://fonts.googleapis.com/css?family=Roboto+Condensed%7CRoboto+Slab:300,400,700%7CRoboto:300,400,500,700"
        rel="stylesheet">
    <!-- Stylesheets -->
    <link rel="stylesheet" href="/css2/bootstrap.min.css">
    <link rel="stylesheet" href="/css2/main.css">
    <link rel="stylesheet" href="/css2/style.css">
    <link rel="stylesheet" href="/css2/colors.css">
    <link rel="stylesheet" href="/css2/responsive.css">
    <link rel="stylesheet" href="/css2/jquery-ui.min.css">
    <link rel="stylesheet" href="/css2/weather-icons.min.css">
    <!--[if lt IE 9]>
      <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
      <![endif]-->
    <link rel="stylesheet" href="../../cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.3/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
</head>

<body>
    <div id="pageloader">
        <div class="loader-item">
            <img src="/img2/load.gif" alt='loader' />
        </div>
    </div>
    <!--========== BEGIN #WRAPPER ==========-->
    <div id="wrapper" data-color="blue">
        <!--========== BEGIN #HEADER ==========-->
        <header id="header">
            <!-- Begin .top-menu -->
            <div class="top-menu" wp-site-content>
                <!-- Begin .container -->
                <div class="container">
                    <!-- Begin .left-top-menu -->
                    <ul class="left-top-menu">
                        <li>
                            <a href="#" class="facebook"><i class="fa fa-facebook"></i></a>
                        </li>
                        <li>
                            <a href="#" class="twitter"><i class="fa fa-twitter"></i></a>
                        </li>
                        <li>
                            <a href="#" class="youtube"> <i class="fa fa-youtube"></i></a>
                        </li>
                        <li>
                            <a href="#" class="google-plus"><i class="fa fa-google-plus"></i></a>
                        </li>
                        <li>
                            <a href="#" class="linkedin"><i class="fa fa-linkedin"></i></a>
                        </li>
                        <li>
                            <a href="#" class="instagram"> <i class="fa fa-instagram"></i></a>
                        </li>
                        <li class="address">
                            <a href="#"><i class="fa fa-phone"></i> +00 (123) 456 7890</a>
                        </li>
                        <li class="address">
                            <a href="#"><i class="fa fa-envelope-o"></i> info@domain.com</a>
                        </li>
                    </ul>
                    <!-- End .left-top-menu -->
                    <!-- Begin .right-top-menu -->
                    <ul class="right-top-menu pull-right">
                        <li class="contact">
                            <a href="contact.html"><i class="fa fa-map-marker fa-i"></i></a>
                        </li>
                        <li class="about">
                            <a href="about-us.html"><i class="fa fa-user fa-i"></i></a>
                        </li>
                        <li>
                            <div class="search-container">
                                <div class="search-icon-btn">
                                    <span style="cursor:pointer"><i class="fa fa-search"></i></span>
                                </div>
                                <div class="search-input">
                                    <input type="search" class="search-bar" placeholder="Search..." title="Search" />
                                </div>
                            </div>
                        </li>
                    </ul>
                    <!-- End .right-top-menu -->
                </div>
                <!-- End .container -->
            </div>
            <!-- End .top-menu -->
            <!-- Begin .container -->
            <div class="container">
                <!-- Begin .header-logo -->
                <div class="header-logo">
                    <a href="/">
                        <img src="/img2/logo.png" alt="Site Logo" />
                        <h1><span style="color: #006cbe;">News</span> <span style="color: #700000;">Plus</span></h1>
                        <h4>We Bring You Truth</h4>
                    </a>
                </div>
                <!-- End .header-logo -->
                <!-- Begin .header-add-place -->
                {{-- <div class="header-add-place">
                    <div class="desktop-add">
                        <a href="https://themeforest.net/item/24h-news-broadcast-news-tv-channel-news-magazine-template/18614179"
                            target="_blank">
                            <img src="/img2/banner_728x90.jpg" alt="">
                        </a>
                    </div>
                </div> --}}
                <!-- End .header-add-place -->
                <!--========== BEGIN .NAVBAR #MOBILE-NAV ==========-->
                <nav class="navbar navbar-default" id="mobile-nav">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse" id="sidenav-toggle">
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <div class="sidenav-header-logo">
                            <a href="/">
                                <img src="/img2/logo.png" alt="Site Logo" />
                                <h1><span style="color: #006cbe;">News</span> <span style="color: #700000;">Plus</span>
                                </h1>
                                <h4>We Bring You Truth</h4>
                            </a>
                        </div>
                    </div>
                    <div class="sidenav" data-sidenav data-sidenav-toggle="#sidenav-toggle">
                        <button type="button" class="navbar-toggle active" data-toggle="collapse">
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <div class="sidenav-brand">
                            <div class="sidenav-header-logo">
                                <a href="index.html">
                                    <img src="/img2/logo.png" alt="Site Logo" />
                                    <h1><span style="color: #006cbe;">News</span> <span
                                            style="color: #700000;">Plus</span></h1>
                                    <h4>We Bring You Truth</h4>
                                </a>
                            </div>
                        </div>
                        <ul class="sidenav-menu">
                            <li>
                                <a href="/" class="active">Home</a>
                                @foreach ($categories as $item)
                                <a href="/category/{{$item->id }}" class="active">{{ $item->name }}</a>
                                @endforeach

                            </li>
                        </ul>
                    </div>
                </nav>
                <!--========== END .NAVBAR #MOBILE-NAV ==========-->
            </div>
            <!-- End .container -->
            <!--========== BEGIN .NAVBAR #FIXED-NAVBAR ==========-->
            <div class="navbar" id="fixed-navbar">
                <!--========== BEGIN MAIN-MENU .NAVBAR-COLLAPSE COLLAPSE #FIXED-NAVBAR-TOOGLE ==========-->
                <div class="main-menu nav navbar-collapse collapse" id="fixed-navbar-toggle">
                    <!--========== BEGIN .CONTAINER ==========-->
                    <div class="container">
                        <!-- Begin .nav navbar-nav -->
                        <ul class="nav navbar-nav">
                            <li class="active">
                                <a href="/">Home</a>
                            </li>
                            @foreach ($categories as $item)
                            <li class="active">
                                <a href="/category/{{$item->id }}">{{ $item->name }}</a>
                            </li>
                            @endforeach
                            <!--========== END DROPDOWN MEGA-DROPDOWN ==========-->
                        </ul>
                        <!--========== END .NAV NAVBAR-NAV ==========-->
                    </div>
                    <!--========== END .CONTAINER ==========-->
                </div>
                <!--========== EsND MAIN-MENU .NAVBAR-COLLAPSE COLLAPSE #FIXED-NAVBAR-TOOGLE ==========-->
                <!--========== BEGIN .SECOND-MENU NAVBAR #NAV-BELOW-MAIN ==========-->
                <div class="second-menu navbar" id="nav-below-main">
                    <!-- Begin .container -->
                    <div class="container">
                        <!-- Begin .collapse navbar-collapse -->
                        <div class="collapse navbar-collapse nav-below-main">
                            <!-- Begin .nav navbar-nav -->
                            <!-- End .nav navbar-nav -->
                        </div>
                        <!-- End .collapse navbar-collapse -->
                        <!-- Begin .clock -->
                        <div class="clock">
                            <div id="time"></div>
                            <div id="date"></div>
                        </div>
                        <!-- End .clock -->
                    </div>
                    <!-- End .container -->
                </div>
                <!--========== END .SECOND-MENU NAVBAR #NAV-BELOW-MAIN ==========-->
            </div>
        </header>
        <!--========== END #HEADER ==========-->

        @yield('content')

        <!--========== BEGIN #FOOTER ==========-->
        <footer id="footer">
            <!-- Begin .parallax -->
            <div id="parallax-section2">
                <div class="bg parallax2 overlay img-overlay2">
                    <div class="container">
                        <div class="row no-gutter">
                            <div class="col-sm-6 col-md-3">
                                <h3 class="title-left title-style03 underline03">About Us</h3>
                                <p class="about-us">When you’re building a website, it’s tempting to get distracted by
                                    all the bells and whistles of the design process and forget all about creating
                                    compelling content.<br>
                                    But having awesome content on your website is crucial to making inbound marketing
                                    work for your business.<br>
                                    We know ... easier said than done. </p>
                                <div class="site-logo"><a href="/"><img src="/img2/logo.png" alt="Side Logo" />
                                        <h3>News <span>Plus</span></h3>
                                        <p>Your 24h News Source</p>
                                    </a></div>
                            </div>
                            <div class="col-sm-6 col-md-3">
                                <h3 class="title-left title-style03 underline03">News</h3>
                                <div class="footer-post">
                                    <ul>
                                        @foreach ($posts as $item)
                                        <li>
                                            <div class="item">
                                                <div class="item-image"><a class="img-link" href="/postdetail/{{ $item->id}}"><img
                                                            class="img-responsive img-full"
                                                            src="/uploads/{{ $item->image }}" alt=""></a></div>
                                                <div class="item-content">
                                                    <p class="ellipsis"><a href="/postdetail/{{ $item->id}}">{{ $item->title }}</a></p>
                                                </div>
                                            </div>
                                        </li>
                                        @endforeach
                                    </ul>
                                </div>
                            </div>
                            <div class="col-sm-6 col-md-3">
                                <h3 class="title-left title-style03 underline03">Categories</h3>
                                <div class="footer-post">
                                    <ul>
                                        @foreach ($categories as $item)
                                        <li>
                                            <div class="item">
                                                <div class="item-image"><a class="img-link" href="/category/{{$item->id }}"><img
                                                            class="img-responsive img-full"
                                                            src="/uploads/{{ $item->image }}" alt=""></a>
                                                </div>
                                                <div class="item-content">
                                                    <p class="ellipsis"><a href="/category/{{$item->id }}">{{ $item->name }}</a></p>
                                                </div>
                                            </div>
                                        </li>
                                        @endforeach
                                    </ul>
                                </div>
                            </div>
                            <div class="col-sm-6 col-md-3">
                                <h3 class="title-left title-style03 underline03">Tags</h3>
                                <div class="tagcloud">
                                    @foreach ($tags as $item)
                                    <a href="#">{{ $item->name }}</a>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- End .parallax -->
        </footer>
        <!--========== END #FOOTER ==========-->
        <!--========== BEGIN #COPYRIGHTS ==========-->
        <div id="copyrights">
            <!-- Begin .container -->
            <div class="container">
                <!-- Begin .copyright -->
                <div class="copyright"> &copy; 2016, Copyrights 24hNews Theme. All Rights Reserved </div>
                <!-- End .copyright -->
                <!--  Begin .footer-social-icons -->
                <div class="footer-social-icons">
                    <ul>
                        <li> <a href="#" class="google-plus"><i class="fa fa-google-plus"></i></a> </li>
                        <li> <a href="#" class="facebook"><i class="fa fa-facebook"></i></a> </li>
                        <li> <a href="#" class="twitter"><i class="fa fa-twitter"></i></a> </li>
                        <li> <a href="#" class="linkedin"><i class="fa fa-linkedin"></i></a> </li>
                    </ul>
                </div>
                <!--  End .footer-social-icons -->
            </div>
            <!-- End .container -->
        </div>
        <!--========== END #COPYRIGHTS ==========-->
    </div>
    <!--========== END #WRAPPER ==========-->

    <!-- External JavaScripts -->
    <script src="/js2/jquery-3.1.1.min.js"></script>
    <script src="/js2/bootstrap.min.js"></script>
    <script src="/js2/jquery-ui.min.js"></script>
    <script src="/js2/plugins.js"></script>

    <!-- JavaScripts -->
    <script src="/js2/functions.js"></script>
</body>

<!-- Mirrored from via-theme.com/24hNews/news.html by HTTrack Website Copier/3.x [XR&CO'2014], Sat, 11 Jan 2020 16:39:30 GMT -->

</html>
