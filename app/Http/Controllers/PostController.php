<?php

namespace App\Http\Controllers;

use App\Post;
use App\Category;
use Illuminate\Http\Request;
use App\Utilities\FileUpload;
use PhpParser\Node\Stmt\Foreach_;

class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $posts = Post::with('category')->paginate();
        return view('post.index', compact('posts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::all();
        return view('post.create', compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->validate([
            'title' => 'required',
            'category_id' => 'required',
            'tag_id' => 'required',
            'image' => 'required',
            'description' => 'required'
        ]);

        $fileupload = new FileUpload();
        $data['image'] = $fileupload->upload($data['image']);

        $post = Post::create($data);
        $post->tags()->sync($data['tag_id']);
        return redirect()->route('posts.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function show(Post $post)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function edit(Post $post)
    {
        $categories = Category::all();
        return view('post.edit', compact('post', 'categories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Post $post)
    {
        $data = $request->validate([
            'title' => 'required',
            'category_id' => 'required',
            'tag_id' => 'nullable',
            'image' => 'nullable',
            'description' => 'required'
        ]);

        if (isset($data['image'])) {
            $fileupload = new FileUpload();
            $data['image'] = $fileupload->upload($data['image']);
        } else {
            $data['image'] = $post->image;
        }
        $post->update($data);
        if (isset($data['tag_id'])) {
            $post->tags()->sync($data['tag_id']);
        } else {
            foreach ($post->tags as  $value) {
                $post->tags()->sync($value->id);
            }
        }

        return redirect()->route('posts.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function destroy(Post $post)
    {
        //
    }
}
