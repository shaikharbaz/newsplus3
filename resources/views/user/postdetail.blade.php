@extends('layouts.userapp')
@section('content')

<!--========== BEGIN #MAIN-SECTION ==========-->
<div id="main-section">
    <!--========== BEGIN .CONTAINER ==========-->
    <div class="container"> </div>
    <!--========== END .CONTAINER ==========-->
    <!--========== BEGIN .MODULE ==========-->
    <section class="module">
      <div class="container">
        <!--========== BEGIN .BREAKING-NEWS ==========-->

        <!--========== BEGIN .ROW ==========-->
        <div class="row no-gutter">
          <!--========== BEGIN .COL-MD-8 ==========-->
          <div class="col-md-8">
            <!--========== BEGIN .POST ==========-->
            <div class="post post-full clearfix">
            <div class="entry-media"> <a href="#"><img src="/uploads/{{ $post->image}}" alt="Foto" class="img-responsive"></a> </div>
              <div class="entry-main">
                <div class="entry-title">
                <h4 class="entry-title"><a href="#">{{ $post->title}}</a></h4>
                </div>


                <div class="entry-content">
                  <p>{!! $post->description !!}</p>

                <div class="news">
                  <!-- Begin .item -->
                  <div class="item">
                      <div class="post-tags"><span class="post-tags_title">tags :</span>@foreach ($post->tags as $item)
                      <a href="#" class="post-tags_link"> {{ $item->name }}</a>
                      @endforeach</div>
                    </div>
                  </div>
                  <!-- End .item -->
                </div>
              </div>
            </div>
            <!--  End .post -->

            <!--  End .author-post -->

            <!--  End .form-reply-section -->
          </div>
          <!--========== END .COL-MD-8 ==========-->
          <!--========== BEGIN .COL-MD-4==========-->
          <div class="col-md-4">
            <!--========== BEGIN #SIDEBAR-NEWSFEED ==========-->
            <!-- Begin .block-title-2 -->

            <!--========== END #SIDEBAR-NEWSFEED ==========-->
             <!--========== BEGIN .TV SCHEDULE ==========-->

            <!--========== END .TV SCHEDULE ==========-->
            <!--========== BEGIN .SIDEBAR-POST ==========-->
            <!-- Begin .title-style02 -->

            <!--========== END .SIDEBAR-POST ==========-->
            <!-- Begin .sidebar-add-place -->
            <div class="sidebar-add-place">
              <a href="#" target="_blank"><img src="/uploads/{{ $banner->image }}" alt=""></a>
            </div>
            <!-- End .sidebar-add-place -->
            <!--========== BEGIN .SIDEBAR-CATEGORIES ==========-->
            <!-- Begin .block-title-1 -->
            <div class="block-title-1 center">
              <h3>Categories</h3>
            </div>
            <!-- End .block-title-1 -->
            <div class="sidebar-categories">
              <ul class="list list-mark-1">
                  @foreach ($categories as $item)
              <li><a href="/category/{{ $item->id}}">{{ $item->name }}</a> <span>{{ $item->posts->count() }}</span> </li>
                  @endforeach
              </ul>
            </div>
            <!--========== END .SIDEBAR-CATEGORIES ==========-->

            <!--========== BEGIN .SIDEBAR-REVIEWS==========-->
            <!-- Begin .block-title-3 -->

            <!--========== END .SIDEBAR-REVIEWS ==========-->

            <!--========== BEGIN .CALENDAR==========-->

    <!--========== END .CONTAINER ==========-->
    </section>
    <!--========== END .MODULE ==========-->
   <!--========== BEGIN .MODULE ==========-->

    <!--========== END .MODULE ==========-->
  </div>
@endsection

